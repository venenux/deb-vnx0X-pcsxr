PCSX-Reloaded 1.9.92+1.9.93
============================

PCSX-Reloaded is a forked version of the dead PCSX emulator, with a nicer
interface and several improvements to stability and functionality.

PCSX-Reloaded uses the PSEMU plugin interface to provide most functionality;
without them, you will not be able to use it to play games. PCSX-Reloaded
provides a number of plugins to provide basic functionality out of the box.

PCSX-Reloaded has a very capable Internal HLE BIOS that can run many games
without problems. It is recommended that you use it. However, if you own a
real PlayStation, you may be able to use your own BIOS image. PCSX-Reloaded
will find it in ~/.pcsxr/bios/ or /usr/share/psemu/bios/ if you place it there.
This can improve compatibility, especially with certain games and with the
use of memory cards.

See the doc/ folder in the source, or /usr/share/doc/pcsxr/ on Debian systems,
for more detailed information on PCSX-Reloaded. A UNIX manpage is also
available.


What its this version?
---------------------

This version its a GTK release with some commits from next release.
The last release that compiles and run with GTK2 are 1.9.92, 
the future releases now runs with newer GTK3 from 1.9.93, 
so the 1.9.93 mayority of changes are for MacOSX, this version are 
a linux GTK2 focused version with some little fixeds from next release.

This version rebranding the good right name to PCSXR event only PCSX!
the next release 1.9.93 include this renaming scheme yet!

This version of emulator are available for VenenuX 0.X release.

For VenenuX 1.X releases we uses the debian and lasted release.

TODO
----

* urgent analize https://github.com/rocket049/PCSX-Reloaded/commit/156f0c6cc32696f9abf750f15703bf53207c2270
* analize and track https://github.com/MaddTheSane/PCSX-Reloaded/commit/0a38beab01653214d0a560e36990e4c113ae7b38
* track gliches of ff7 https://github.com/bblack/PCSX-Reloaded/commit/c5f69c2e4317d60e5e142f0bbb945d8fa3de6e4b
* track possible https://github.com/bblack/PCSX-Reloaded/commit/a86d2e1acedd387cd0a9726fa1e628bc5355f7fd
* track of https://github.com/bblack/PCSX-Reloaded/commit/ea833dff44e48617df3be7c44f0d3119bbb37654
* important https://github.com/bblack/PCSX-Reloaded/commit/d90da6f0d763d68293c528f678098935acb04fe5#diff-7509f4d8f1c5c16fda99404436a18b42R387

repos work>

* unix related only https://github.com/bblack/PCSX-Reloaded
* mac os only https://github.com/MaddTheSane/PCSX-Reloaded/
* linux related fix https://github.com/iCatButler/pcsxr/


